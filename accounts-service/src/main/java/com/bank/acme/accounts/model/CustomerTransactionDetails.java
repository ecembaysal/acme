package com.bank.acme.accounts.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.List;

@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CustomerTransactionDetails {
  private Integer customerId;
  private String customerFullName;
  private List<AccountTransaction> accountTransactions;

}
