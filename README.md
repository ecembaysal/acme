# Getting Started with AcmeBank!
This is an assessment which consists of an API to be used for opening a new “current account” of already existing customers.


## General Info About Technical Specifications 
The application is a maven based Spring-Boot application that connects to H2 in-memory database. It consists of 
one module which has a Spring Boot application named "account-service".

## How to run the application?
Since Java 11 is used to build this application, please make sure that you have at least Java 11 installed on your computer.
There isn't a specific profile that you should set, so simply run the application with following command:

```bash
mvn install
cd accounts-service
java -jar target/accounts-service-0.0.1-SNAPSHOT.jar
```


## Run it with Docker
There's a Dockerfile defined for this application, and you can build and use the image that's created. A simpler way is 
to easily run a docker-compose command to run it, like below:
```bash
docker-compose up -d
```

## How to test?
Test the application by using POSTMAN functions.

### Requirements

 - Add a new customer: This will lead the application to create a customer as well as an account linked to them.
 - Add a new account: This will lead a Deposit typed transaction to be triggered in case the given initialAmount is more than 0.
 - List accounts for customer and transactions by accounts


## Details of AcmeBank's Backend Application

In order to create an account, one should first create a new customer since database content will be started clean when the application starts.
http://localhost:8080/customers/new
```json
{
    "name": "Tom",
    "surname": "Acme",
    "residenceCountry": "NL"
}
```

AcmeBank uses customers’ country information to find currency code when adding a new account. USA, NL, DE, FR can be stated as the residenceCountry information in the application.

AcmeBanks also assumes that every customer has a current account by default, therefore creating a customer will trigger adding an account for the brand new customer whose initial credit is 0.

After adding new customers, the whole customer list of AcmeBank can be seen using the endpoint below.
http://localhost:8080/customers/all


If one needs to see a customer information individually then, getCustomerById method can be used to get customers personal information. First customers id value will be 1000.
http://localhost:8080/customers/1000



To create a new account, endpoint stated below can be used. If the initial credit value is above zero, a transaction will be sent to the new account. In AcmeBank users pay the initial amount of the balance by cash, thus, the transaction type will be saved as ‘Deposit’ each time a new account is created by default. Also, it is not possible in AcmeBank to start a new account with a balance below zero and only iban is being used as a unique value on Account table.
http://localhost:8080/accounts/new
```json
{
    "customerId": 1000,
    "balance": 2250.00
}
```

In order to see only account-based transaction information this endpoint can be used with accounts Iban.
http://localhost:8080/transactions/all/account/NL07RZZY3595894390


Added account list of an existing customer can be returned using this endpoint.
http://localhost:8080/accounts/all/customer/1000


This endpoint is created to output user information with transaction details of related accounts.
http://localhost:8080/accounts/transaction/customerId/?customerId=1000




## H2 Database information
http://localhost:8080/h2-console/

JDBC URL: jdbc:h2:mem:acmebankdb
User Name : admin
Password : admin