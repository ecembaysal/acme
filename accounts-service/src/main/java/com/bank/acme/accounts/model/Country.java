package com.bank.acme.accounts.model;

import lombok.Getter;
import org.springframework.util.StringUtils;

@Getter
public enum Country {
  USA("United States", "USA", Currency.USD),
  NL("Netherlands", "NL", Currency.EUR),
  DE("Germany", "DE", Currency.EUR),
  FR("France", "FR", Currency.EUR);

  private String name;
  private String code;
  private Currency currency;

  Country(String name, String code, Currency currency) {
    this.name = name;
    this.code = code;
    this.currency = currency;
  }

  /**
   * Default country is {@link Country#NL}, which is Netherlands and the default Currency will be {@link Currency#EUR}
   * @param code CountryCode
   * @return {@link Country}
   */
  public static Country getCountryByCode(String code) {
    if (StringUtils.isEmpty(code)) {
      return NL;
    }
    return Enum.valueOf(Country.class, code);
  }

}
