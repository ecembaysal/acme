package com.bank.acme.accounts.resource;

import com.bank.acme.accounts.exception.AccountNotFoundException;
import com.bank.acme.accounts.model.Transaction;
import com.bank.acme.accounts.service.AccountService;
import com.bank.acme.accounts.service.TransactionService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;
import java.util.List;

@RestController
@RequestMapping("/transactions")
@AllArgsConstructor
public class TransactionResource {

  private final TransactionService transactionService;
  private final AccountService accountService;

  @GetMapping(path = "/all/account/{iban}")
  public List<Transaction> getTransactionHistoryByIban(@PathVariable String iban) {
    List<Transaction> transactions = transactionService.getTransactionHistoryByIban(iban);
    return transactions;
  }

  @PutMapping(path = "/new")
  public Transaction addNewTransaction(@RequestBody @NotNull Transaction transaction) {

    if (accountService.accountWithIbanExists(transaction.getToIban())) {
      throw new AccountNotFoundException(transaction.getToIban());
    }
    return transactionService.saveTransaction(transaction);
  }
}
