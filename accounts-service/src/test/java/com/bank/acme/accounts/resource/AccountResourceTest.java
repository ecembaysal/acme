package com.bank.acme.accounts.resource;


import com.bank.acme.accounts.model.*;
import com.bank.acme.accounts.service.AccountService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.util.NestedServletException;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Collections;
import java.util.List;

import static java.lang.String.format;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(AccountResource.class)
public class AccountResourceTest {

  @MockBean
  private AccountService accountService;

  @Autowired
  private WebApplicationContext webApplicationContext;

  private MockMvc mockMvc;


  @Before
  public void setup() {
    this.mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
  }

  @Test
  public void getAllAccountsByCustomerId() throws Exception {
    List<Account> accounts = Collections.singletonList(createAccount());
    when(accountService.getAllAccountsByCustomerId(any())).thenReturn(accounts);

    ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders.get(format("/accounts/all/customer/%d", 12345))
        .accept(MediaType.APPLICATION_JSON));

    resultActions.andExpect(status().isOk());

    for (int i = 0; i < accounts.size(); i++) {
      resultActions.andExpect(jsonPath(format("$.[%d].balance", i)).value(accounts.get(i).getBalance()))
          .andExpect(jsonPath(format("$.[%d].currency", i)).value(accounts.get(i).getCurrency().name()))
          .andExpect(jsonPath(format("$.[%d].accountOpeningDate", i)).value(accounts.get(i).getAccountOpeningDate().toString()))
          .andExpect(jsonPath(format("$.[%d].iban", i)).value(accounts.get(i).getIban()));
    }
  }

  @Test
  public void getCurrentAccountsByCustomerId() throws Exception {
    CustomerAccountDetails customerAccountDetails = CustomerAccountDetails.builder()
        .accounts(Collections.singletonList(createAccount()))
        .customerFullName("Customer Full Name")
        .build();
    when(accountService.getCurrentAccountsByCustomerId(any())).thenReturn(customerAccountDetails);

    ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders.get(format("/accounts/current-account/customer/%d", 12345))
        .accept(MediaType.APPLICATION_JSON));

    resultActions.andExpect(status().isOk())
        .andExpect(jsonPath("$.customerFullName").value(customerAccountDetails.getCustomerFullName()));

    for (int i = 0; i < customerAccountDetails.getAccounts().size(); i++) {
      resultActions.andExpect(jsonPath(format("$.accounts[%d].balance", i)).value(customerAccountDetails.getAccounts().get(i).getBalance()))
          .andExpect(jsonPath(format("$.accounts[%d].currency", i)).value(customerAccountDetails.getAccounts().get(i).getCurrency().name()))
          .andExpect(jsonPath(format("$.accounts[%d].accountOpeningDate", i)).value(customerAccountDetails.getAccounts().get(i).getAccountOpeningDate().toString()))
          .andExpect(jsonPath(format("$.accounts[%d].iban", i)).value(customerAccountDetails.getAccounts().get(i).getIban()));
    }

  }

  @Test
  public void getAccountByIban() throws Exception {
    Account account = createAccount();
    when(accountService.getAccountByIban(any())).thenReturn(account);

    ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders.get(format("/accounts/account/iban/%s", "NL1234567890"))
        .accept(MediaType.APPLICATION_JSON));

    resultActions.andExpect(status().isOk())
        .andExpect(jsonPath("$.accountType").value(account.getAccountType().name()))
        .andExpect(jsonPath("$.balance").value(account.getBalance()))
        .andExpect(jsonPath("$.currency").value(account.getCurrency().name()))
        .andExpect(jsonPath("$.accountOpeningDate").value(account.getAccountOpeningDate().toString()))
        .andExpect(jsonPath("$.iban").value(account.getIban()));

  }

  @Test
  public void createCurrentAccountWithDefaultBalance() throws Exception {
    NewAccountInfo newAccountInfo = new NewAccountInfo(10, null);
    ObjectMapper objectMapper = new ObjectMapper();
    String body = objectMapper.writeValueAsString(newAccountInfo);

    Account account = createAccount();
    when(accountService.createCurrentAccount(any(), any())).thenReturn(account);

    ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders.put("/accounts/new")
        .content(body)
        .contentType(MediaType.APPLICATION_JSON)
        .accept(MediaType.APPLICATION_JSON));

    resultActions.andExpect(status().isOk())
        .andExpect(jsonPath("$.accountType").value(account.getAccountType().name()))
        .andExpect(jsonPath("$.balance").value(account.getBalance()))
        .andExpect(jsonPath("$.currency").value(account.getCurrency().name()))
        .andExpect(jsonPath("$.accountOpeningDate").value(account.getAccountOpeningDate().toString()))
        .andExpect(jsonPath("$.iban").value(account.getIban()));
  }

  @Test(expected = NestedServletException.class)
  public void createCurrentAccountWithNegativeBalance() throws Exception {
    NewAccountInfo newAccountInfo = new NewAccountInfo(10, new BigDecimal(-10));
    ObjectMapper objectMapper = new ObjectMapper();
    String body = objectMapper.writeValueAsString(newAccountInfo);

    mockMvc.perform(MockMvcRequestBuilders.put("/accounts/new")
        .content(body)
        .contentType(MediaType.APPLICATION_JSON)
        .accept(MediaType.APPLICATION_JSON));
  }

  private Account createAccount() {
    return Account.builder()
        .accountType(AccountType.CURRENT_ACCOUNT)
        .balance(BigDecimal.TEN)
        .currency(Currency.EUR)
        .accountOpeningDate(LocalDate.now())
        .iban("NL1234567890")
        .build();
  }
}