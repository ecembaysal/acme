package com.bank.acme.accounts.exception;

public class CustomerNotFoundException extends RuntimeException {
  public CustomerNotFoundException(Integer customerId) {
    super("Customer with id " + customerId + " is not found!");
  }
}
