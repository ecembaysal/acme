package com.bank.acme.accounts.service;

import com.bank.acme.accounts.exception.CustomerNotFoundException;
import com.bank.acme.accounts.model.Customer;
import com.bank.acme.accounts.repository.CustomerRepository;
import lombok.AllArgsConstructor;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@Slf4j
@AllArgsConstructor
public class CustomerService {

  private final CustomerRepository customerRepository;

  public List<Customer> getAllCustomers() {
    return customerRepository.findAll();
  }

  public Customer getCustomerById(@NonNull Integer customerId) {
    Optional<Customer> customer = customerRepository.findById(customerId);
    if (customer.isEmpty()) {
      throw new CustomerNotFoundException(customerId);
    }
    return customer.get();
  }

  /**
   * Returns recently created customer. Assuming all customers has at least one current account by default.
   *
   * @param customer
   * @return new Customer
   */
  public Customer createCustomer(Customer customer) {
    Customer newCustomer = customerRepository.save(customer);
    log.info("New customer is created. Customer ID: " + newCustomer.getId());
    return newCustomer;
  }
}
