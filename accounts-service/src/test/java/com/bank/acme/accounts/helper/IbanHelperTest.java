package com.bank.acme.accounts.helper;

import com.bank.acme.accounts.model.Country;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class IbanHelperTest {

  @Test
  void createMockIban() {
    String mockIban = IbanHelper.createMockIban(Country.NL, "000011111");
    assertThat(mockIban).isNotEmpty();
  }
  @Test
  void createMockIban_random() {
    String mockIban = IbanHelper.createMockIban();
    assertThat(mockIban).isNotEmpty();
  }
}