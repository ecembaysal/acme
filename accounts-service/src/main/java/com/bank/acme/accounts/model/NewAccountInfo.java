package com.bank.acme.accounts.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

/**
 * Used in creating a current account to receive necessary information to create a new account
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class NewAccountInfo {
  private Integer customerId;
  private BigDecimal balance;
}
