package com.bank.acme.accounts.service;

import com.bank.acme.accounts.exception.AccountNotFoundException;
import com.bank.acme.accounts.exception.CustomerNotFoundException;
import com.bank.acme.accounts.model.*;
import com.bank.acme.accounts.repository.AccountRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.bank.acme.accounts.helper.CurrencyHelper.getRelevantCurrencyForCountry;
import static com.bank.acme.accounts.helper.IbanHelper.createMockIban;

@Service
@Slf4j
@AllArgsConstructor
public class AccountService {

  private final AccountRepository accountRepository;
  private final CustomerService customerService;
  private final TransactionService transactionService;

  public List<Account> getAllAccountsByCustomerId(Integer customerId) {
    return accountRepository.findAllByCustomerId(customerId);
  }

  /**
   * Returns {@link CustomerAccountDetails} of current accounts for given customer by customerID.
   * Calls {@link AccountRepository} and {@link CustomerService} to retrieve necessary data and filters accounts by type.
   *
   * @param customerId
   * @return {@link CustomerAccountDetails} which contains brief customer info and current-accounts list
   */
  public CustomerAccountDetails getCurrentAccountsByCustomerId(Integer customerId) {
    Customer customer = customerService.getCustomerById(customerId);
    List<Account> currentAccounts = retrieveCurrentAccountsByCustomerId(customerId);
    return CustomerAccountDetails.builder().customerId(customerId).customerFullName(customer.getFullName()).accounts(currentAccounts).build();
  }

  public boolean accountWithIbanExists(String iban) {
    return accountRepository.findById(iban).isPresent();
  }

  public Account getAccountByIban(String iban) throws AccountNotFoundException {
    Optional<Account> account = accountRepository.findById(iban);
    if (account.isEmpty()) {
      throw new AccountNotFoundException(iban);
    }
    return account.get();
  }

  /**
   * Creates a new current account for the customer with given customerId.
   * We assume that the amount is transferred to our bank by deposit.
   * Throws {@link CustomerNotFoundException} in case the given customer does not exist in DB!
   *
   * @param customerId
   * @param balance
   * @return newly created {@link Account}
   */
  @Transactional
  public Account createCurrentAccount(Integer customerId, BigDecimal balance) {
    // This call also validates that the customer already exists
    Customer customer = customerService.getCustomerById(customerId); //cover customer not existing case with unit tests!

    Account newAccount = addNewAccount(customer, balance);
    // Transaction requirement
    if (balance.compareTo(BigDecimal.ZERO) > 0) {

      Transaction transaction = Transaction.builder()
          .transactionType(TransactionType.DEPOSIT)
          .transferTime(LocalDateTime.now())
          .toIban(newAccount.getIban())
          .transferValue(balance)
          .build();

      transactionService.saveTransaction(transaction);
      log.info(TransactionType.DEPOSIT.name() + " Transaction is saved. Transaction ID : " + transaction.getTransactionId() + " to new Account ID : " + newAccount.getIban());
    }

    return newAccount;
  }

  public CustomerTransactionDetails getTransactionsByCustomer(Integer customerId) {
    if (customerId.equals(null)) {
      throw new CustomerNotFoundException(customerId);
    }
    Customer customer = customerService.getCustomerById(customerId);
    List<Account> currentAccounts = retrieveCurrentAccountsByCustomerId(customerId);
    List<AccountTransaction> accountTransactions = new ArrayList<>();
    for (Account account : currentAccounts) {
      AccountTransaction accountTransaction = retrieveAccountTransactionsByIban(account.getIban());
      accountTransactions.add(accountTransaction);
    }
    return CustomerTransactionDetails.builder()
        .customerId(customerId)
        .customerFullName(customer.getFullName())
        .accountTransactions(accountTransactions)
        .build();
  }

  private List<Account> retrieveCurrentAccountsByCustomerId(Integer customerId) {
    List<Account> accounts = accountRepository.findAllByCustomerId(customerId);
    return accounts.stream().filter(account -> account.getAccountType().equals(AccountType.CURRENT_ACCOUNT)).collect(Collectors.toList());
  }

  private AccountTransaction retrieveAccountTransactionsByIban(String iban) {
    Optional<Account> account = accountRepository.findById(iban);
    List<Transaction> transactions = transactionService.getTransactionHistoryByIban(iban);
    return AccountTransaction.builder()
        .account(account.get())
        .transactions(transactions)
        .build();
  }


  private Account addNewAccount(Customer customer, BigDecimal balance) {
    Account addedAccount = accountRepository.save(Account.builder()
        .customerId(customer.getId())
        .iban(createMockIban()).
            currency(getRelevantCurrencyForCountry(customer.getResidenceCountry()))
        .accountType(AccountType.CURRENT_ACCOUNT).
            accountOpeningDate(LocalDate.now())
        .balance(balance)
        .build());
    log.info("New account is saved. Account ID : " + addedAccount.getIban() + " for customer " +
        customer.getFullName() + " with ID : " + customer.getId() + ". Balance : " + balance);
    return addedAccount;
  }
}
