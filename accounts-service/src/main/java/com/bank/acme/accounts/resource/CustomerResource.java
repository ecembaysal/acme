package com.bank.acme.accounts.resource;

import com.bank.acme.accounts.model.Account;
import com.bank.acme.accounts.model.Customer;
import com.bank.acme.accounts.model.CustomerAccountDetails;
import com.bank.acme.accounts.service.AccountService;
import com.bank.acme.accounts.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;

@RestController
@RequestMapping("/customers")
public class CustomerResource {

  @Autowired
  private CustomerService customerService;
  @Autowired
  private AccountService accountService;

  @GetMapping(path = "/all")
  public List<Customer> getAllCustomers() {
    return customerService.getAllCustomers();
  }

  @GetMapping(path = "/{customerId}")
  public Customer getCustomerById(@PathVariable Integer customerId) {
    return customerService.getCustomerById(customerId);
  }

  /**
   * On-boarding a new customer simply means that the new customer has a new current-account, thus we immediately
   * create an account for them.
   *
   * @param customer
   * @return ResponseEntity
   */
  @PutMapping(path = "/new")
  public ResponseEntity createCustomer(@RequestBody Customer customer) {
    Customer newCustomer = customerService.createCustomer(customer);
    Account newAccount = accountService.createCurrentAccount(customer.getId(), BigDecimal.valueOf(0.00));
    CustomerAccountDetails newCustomerAccountDetails = CustomerAccountDetails.builder().customerId(newCustomer.getId())
        .customerFullName(newCustomer.getFullName()).accounts(Collections.singletonList(newAccount)).build();
    return ResponseEntity.ok(newCustomerAccountDetails);
  }

}
