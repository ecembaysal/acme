package com.bank.acme.accounts.resource;

import com.bank.acme.accounts.exception.BalanceNegativeException;
import com.bank.acme.accounts.model.Account;
import com.bank.acme.accounts.model.CustomerAccountDetails;
import com.bank.acme.accounts.model.CustomerTransactionDetails;
import com.bank.acme.accounts.model.NewAccountInfo;
import com.bank.acme.accounts.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.List;

@RestController
@RequestMapping("/accounts")
public class AccountResource {

  @Autowired
  private AccountService accountService;

  @GetMapping(path = "/all/customer/{customerId}")
  public List<Account> getAllAccountsByCustomerId(@PathVariable Integer customerId) {
    return accountService.getAllAccountsByCustomerId(customerId);
  }

  @GetMapping(path = "/current-account/customer/{customerId}")
  public CustomerAccountDetails getCurrentAccountsByCustomerId(@PathVariable Integer customerId) {
    return accountService.getCurrentAccountsByCustomerId(customerId);
  }

  @GetMapping(path = "/account/iban/{iban}")
  public Account getAccountByIban(@PathVariable String iban) {
    return accountService.getAccountByIban(iban);
  }

  @PutMapping(path = "/new")
  public ResponseEntity<Account> createCurrentAccount(@RequestBody NewAccountInfo newAccountInfo) {
    if (newAccountInfo.getBalance() == null) {
      newAccountInfo.setBalance(BigDecimal.valueOf(0.00));
    }
    if (newAccountInfo.getBalance().compareTo(BigDecimal.ZERO) < 0) {
      throw new BalanceNegativeException(newAccountInfo.getBalance());
    }
    return ResponseEntity.ok(accountService.createCurrentAccount(newAccountInfo.getCustomerId(), newAccountInfo.getBalance()));
  }

  @GetMapping(path = "/transaction/{customerId}")
  public CustomerTransactionDetails getTransactionDetailsByCustomer(Integer customerId) {
    return accountService.getTransactionsByCustomer(customerId);
  }
}
