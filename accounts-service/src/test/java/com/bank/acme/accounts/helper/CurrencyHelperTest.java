package com.bank.acme.accounts.helper;

import com.bank.acme.accounts.model.Country;
import com.bank.acme.accounts.model.Currency;
import org.junit.jupiter.api.Test;

import static com.bank.acme.accounts.helper.CurrencyHelper.getRelevantCurrencyForCountry;
import static org.assertj.core.api.Assertions.assertThat;

class CurrencyHelperTest {

  @Test
  void getRelevantCurrencyForCountry_givenCountryThenReturnRelevantCurrency() {
    assertThat(getRelevantCurrencyForCountry(Country.NL)).isEqualTo(Currency.EUR);
    assertThat(getRelevantCurrencyForCountry(Country.FR)).isEqualTo(Currency.EUR);
    assertThat(getRelevantCurrencyForCountry(Country.DE)).isEqualTo(Currency.EUR);
    assertThat(getRelevantCurrencyForCountry(Country.USA)).isEqualTo(Currency.USD);
  }
  @Test
  void getRelevantCurrencyForCountry_givenNullCountryThenReturnDefaultCurrency() {
    assertThat(getRelevantCurrencyForCountry(null)).isEqualTo(Currency.EUR);
  }
}