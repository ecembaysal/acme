package com.bank.acme.accounts.service;

import com.bank.acme.accounts.model.Transaction;
import com.bank.acme.accounts.model.TransactionType;
import com.bank.acme.accounts.repository.TransactionRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TransactionServiceTest {

  private static final String IBAN = "NL30INGB4040404041";

  @InjectMocks
  private TransactionService transactionService;

  @Mock
  private TransactionRepository transactionRepository;

  @Test
  public void getTransactionHistoryByIban_retrieveAllTransactions() {

    when(transactionRepository.findAllByFromIban(IBAN)).thenReturn(Collections.singletonList(Transaction.builder().transactionType(TransactionType.DEPOSIT).fromIban(IBAN).transferTime(LocalDateTime.now()).transferValue(BigDecimal.TEN).transactionId(UUID.randomUUID()).build()));
    when(transactionRepository.findAllByToIban(IBAN)).thenReturn(Collections.singletonList(Transaction.builder().transactionType(TransactionType.DEPOSIT).toIban(IBAN).transferTime(LocalDateTime.now()).transferValue(BigDecimal.TEN).transactionId(UUID.randomUUID()).build()));
    List<Transaction> resultTransactions = transactionService.getTransactionHistoryByIban(IBAN);
    assertThat(resultTransactions).isNotNull().isNotEmpty().hasSize(2);
  }

  @Test
  public void getTransactionHistoryByIban_whenNoFromIbanTransactionsThenOnlyRetrieveToIbanTransactions() {
    when(transactionRepository.findAllByFromIban(IBAN)).thenReturn(Collections.emptyList());
    when(transactionRepository.findAllByToIban(IBAN)).thenReturn(Collections.singletonList(Transaction.builder().transactionType(TransactionType.DEPOSIT).toIban(IBAN).transferTime(LocalDateTime.now()).transferValue(BigDecimal.TEN).transactionId(UUID.randomUUID()).build()));
    List<Transaction> resultTransactions = transactionService.getTransactionHistoryByIban(IBAN);
    assertThat(resultTransactions).isNotNull().isNotEmpty().hasSize(1);
  }

  @Test
  public void addNewTransaction() {
    Transaction transaction = Transaction.builder().transactionType(TransactionType.DEPOSIT).toIban(IBAN).transferTime(LocalDateTime.now()).transferValue(BigDecimal.TEN).transactionId(UUID.randomUUID()).build();
    when(transactionRepository.save(any(Transaction.class))).thenReturn(transaction);

    Transaction resultTransaction = transactionService.saveTransaction(transaction);
    assertThat(resultTransaction).isEqualTo(transaction);
  }
}
