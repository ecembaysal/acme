package com.bank.acme.accounts.repository;

import com.bank.acme.accounts.model.Account;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AccountRepository extends JpaRepository<Account, String> {

  List<Account> findAllByCustomerId(Integer customerId);

}
