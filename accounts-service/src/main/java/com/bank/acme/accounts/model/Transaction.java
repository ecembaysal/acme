package com.bank.acme.accounts.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.UUID;

@Entity
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Transaction {

  @Id
  private UUID transactionId;

  /**
   * Type of transactions. As the initial implementation supports, it can only be TransactionType.DEPOSIT right now.
   */
  private TransactionType transactionType;

  /**
   *  The account that the money transaction is made from. So far in the project transferring money is not supported
   */
  private String fromIban;

  /**
   *  The account-id (iban) that the money is sent to. So far in the project we set the balance toIban by deposit
   */
  private String toIban;

  /**
   * The amount of money that's sent fromIban to toIban. We use this value to set the initial balance of the new current-accounts.
   */
  private BigDecimal transferValue;

  private LocalDateTime transferTime;
}
