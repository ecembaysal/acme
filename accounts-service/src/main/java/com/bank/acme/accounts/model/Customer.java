package com.bank.acme.accounts.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

/**
 * Main customer information
 */
@Getter
@Entity
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Customer {
  @SequenceGenerator(name = "customerIdSeqGen", sequenceName = "customerIdSeq", initialValue = 1000, allocationSize = 100)
  @GeneratedValue(generator = "customerIdSeqGen")
  @Id
  private Integer id; //CustomerId or Id

  private String name;
  private String surname;

  /**
   * Residence country is where our customer lives in, and it means they use the related currency of this country
   */
  private Country residenceCountry;

  public String getFullName() {
    String fullName = this.name + " " + this.surname;
    return fullName.toUpperCase();
  }
}
