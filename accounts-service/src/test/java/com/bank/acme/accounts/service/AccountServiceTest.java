package com.bank.acme.accounts.service;

import com.bank.acme.accounts.exception.AccountNotFoundException;
import com.bank.acme.accounts.model.*;
import com.bank.acme.accounts.repository.AccountRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.tuple;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class AccountServiceTest {
  private static final String CUSTOMER_NAME = "Tom";
  private static final String CUSTOMER_SURNAME = "Acme";
  private static final Country CUSTOMER_RESIDENCE_COUNTRY = Country.NL;

  @InjectMocks
  private AccountService accountService;

  @Mock
  private AccountRepository accountRepository;

  @Mock
  private CustomerService customerService;

  @Test
  public void getAllAccountsByCustomerId() {
    Account account = createAccount();
    when(accountRepository.findAllByCustomerId(anyInt())).thenReturn(Collections.singletonList(account));

    List<Account> allAccountsByCustomerId = accountService.getAllAccountsByCustomerId(12345);

    assertThat(allAccountsByCustomerId).isNotNull();
    assertThat(allAccountsByCustomerId).extracting(
        Account::getIban,
        Account::getBalance,
        Account::getAccountOpeningDate,
        Account::getCurrency,
        Account::getAccountType)
        .contains(tuple(account.getIban(), account.getBalance(), account.getAccountOpeningDate(), account.getCurrency(), account.getAccountType()));
  }

  @Test
  public void getCurrentAccountsByCustomerId() {
    Customer customer = Customer.builder().name(CUSTOMER_NAME).surname(CUSTOMER_SURNAME).residenceCountry(CUSTOMER_RESIDENCE_COUNTRY).build();
    Account account = createAccount();
    when(customerService.getCustomerById(anyInt())).thenReturn(customer);
    when(accountRepository.findAllByCustomerId(anyInt())).thenReturn(Collections.singletonList(account));

    CustomerAccountDetails currentAccountsByCustomerId = accountService.getCurrentAccountsByCustomerId(12345);

    assertThat(currentAccountsByCustomerId).isNotNull();
    assertThat(currentAccountsByCustomerId.getCustomerFullName()).isEqualTo(CUSTOMER_NAME.concat(" ").concat(CUSTOMER_SURNAME).toUpperCase());
    assertThat(currentAccountsByCustomerId.getAccounts()).extracting(
        Account::getIban,
        Account::getBalance,
        Account::getAccountOpeningDate,
        Account::getCurrency,
        Account::getAccountType)
        .contains(tuple(account.getIban(), account.getBalance(), account.getAccountOpeningDate(), account.getCurrency(), account.getAccountType()));

  }

  @Test
  public void accountWithIbanExists() {
    when(accountRepository.findById(any())).thenReturn(Optional.of(createAccount()));

    boolean withIbanExists = accountService.accountWithIbanExists("NL1234567890");

    assertThat(withIbanExists).isTrue();
  }

  @Test
  public void accountWithIbanExists_WhenAccountNotExistThenFalse() {
    when(accountRepository.findById(any())).thenReturn(Optional.empty());

    boolean withIbanExists = accountService.accountWithIbanExists("NL1234567890");

    assertThat(withIbanExists).isFalse();
  }

  @Test
  public void getAccountByIban() {
    Optional<Account> account = Optional.of(createAccount());

    when(accountRepository.findById(any())).thenReturn(account);

    Account accountByIban = accountService.getAccountByIban("NL1234567890");
    assertThat(accountByIban).isNotNull();
    assertThat(accountByIban).extracting(
        Account::getIban,
        Account::getBalance,
        Account::getAccountOpeningDate,
        Account::getCurrency,
        Account::getAccountType)
        .contains(account.get().getIban(), account.get().getBalance(), account.get().getAccountOpeningDate(), account.get().getCurrency(), account.get().getAccountType());

  }

  @Test(expected = AccountNotFoundException.class)
  public void getAccountByIban_WhenAccountIsNotFoundThenThrowException() {

    when(accountRepository.findById(any())).thenReturn(Optional.empty());

    accountService.getAccountByIban("NL1234567890");

  }

  private Account createAccount() {
    return Account.builder()
        .accountType(AccountType.CURRENT_ACCOUNT)
        .balance(BigDecimal.TEN)
        .currency(Currency.EUR)
        .accountOpeningDate(LocalDate.now())
        .iban("NL1234567890")
        .build();
  }
}
