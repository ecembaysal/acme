package com.bank.acme.accounts.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import java.util.List;


@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class AccountTransaction {

  private Account account;
  private List<Transaction> transactions;
}
