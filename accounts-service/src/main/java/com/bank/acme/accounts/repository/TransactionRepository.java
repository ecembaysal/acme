package com.bank.acme.accounts.repository;

import com.bank.acme.accounts.model.Transaction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface TransactionRepository extends JpaRepository<Transaction, UUID> {

  List<Transaction> findAllByFromIban(String iban);

  List<Transaction> findAllByToIban(String iban);

  ;
}
