package com.bank.acme.accounts.model;

public enum Currency {
  /**
   * Currency code of Euro
   */
  EUR,
  /**
   * Currency code of American Dollar
   */
  USD
}
