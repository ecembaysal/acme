package com.bank.acme.accounts.exception;

public class AccountNotFoundException extends RuntimeException {
  public AccountNotFoundException(String iban) {
    super("Account with iban " + iban + " is not found!");
  }
}
