package com.bank.acme.accounts.model;

public enum AccountType {
  CURRENT_ACCOUNT,
  MORTGAGE_ACCOUNT,
  SAVING_ACCOUNT
}
