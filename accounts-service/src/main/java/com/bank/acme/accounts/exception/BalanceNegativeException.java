package com.bank.acme.accounts.exception;

import java.math.BigDecimal;

public class BalanceNegativeException extends IllegalArgumentException {

  public BalanceNegativeException(BigDecimal balance) {
    super("Balance value cannot be negative! Balance: " + balance);
  }
}
