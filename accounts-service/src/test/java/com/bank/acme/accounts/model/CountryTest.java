package com.bank.acme.accounts.model;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class CountryTest {

  @Test
  void getCountryByCode() {

    assertThat(Country.getCountryByCode("NL")).isEqualTo(Country.NL);
  }

  @Test
  void getName() {
    assertThat(Country.FR.getName()).isEqualTo("France");
    assertThat(Country.NL.getName()).isEqualTo("Netherlands");
  }

  @Test
  void getCode() {
    assertThat(Country.FR.getCode()).isEqualTo("FR");
    assertThat(Country.NL.getCode()).isEqualTo("NL");
  }

  @Test
  void getCurrency() {
    assertThat(Country.NL.getCurrency()).isEqualTo(Currency.EUR);
    assertThat(Country.FR.getCurrency()).isEqualTo(Currency.EUR);
    assertThat(Country.USA.getCurrency()).isEqualTo(Currency.USD);

  }
}