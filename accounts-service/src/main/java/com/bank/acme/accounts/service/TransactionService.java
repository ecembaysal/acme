package com.bank.acme.accounts.service;

import com.bank.acme.accounts.model.Transaction;
import com.bank.acme.accounts.repository.TransactionRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
@AllArgsConstructor
public class TransactionService {

  private final TransactionRepository transactionRepository;

  /**
   * Retrieve all transactions, both made from a specific account (fromIban) and to a specific account (toIban)
   *
   * @param iban
   * @return List of all Transactions that are made related to given iban
   */
  public List<Transaction> getTransactionHistoryByIban(String iban) {
    List<Transaction> allTransactions = new ArrayList<>();

    allTransactions.addAll(transactionRepository.findAllByFromIban(iban));
    allTransactions.addAll(transactionRepository.findAllByToIban(iban));

    return allTransactions;
  }

  public Transaction saveTransaction(Transaction transaction) {
    Transaction newTransaction = Transaction.builder()
        .transactionId(UUID.randomUUID())
        .transactionType(transaction.getTransactionType())
        .transferValue(transaction.getTransferValue())
        .fromIban(transaction.getFromIban())
        .toIban(transaction.getToIban())
        .transferTime(LocalDateTime.now())
        .build();

    return transactionRepository.save(newTransaction);
  }
}
