package com.bank.acme.accounts.helper;

import com.bank.acme.accounts.model.Country;
import org.iban4j.CountryCode;
import org.iban4j.Iban;

public class IbanHelper {

  private static final String BANK_CODE_ACME = "ACME0"; // Bank code specific for Acme Bank!

  public static String createMockIban(Country residenceCounty, String accountNumber) {
    return new Iban.Builder()
        .countryCode(CountryCode.getByCode(residenceCounty.getCode()))
        .bankCode(BANK_CODE_ACME)
        .accountNumber(accountNumber)
        .build().toString();
  }
  public static String createMockIban() {
    return Iban.random(CountryCode.NL).toString();
  }

}
