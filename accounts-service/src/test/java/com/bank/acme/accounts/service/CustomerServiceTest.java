package com.bank.acme.accounts.service;

import com.bank.acme.accounts.exception.CustomerNotFoundException;
import com.bank.acme.accounts.model.Country;
import com.bank.acme.accounts.model.Customer;
import com.bank.acme.accounts.repository.CustomerRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class CustomerServiceTest {

  private static final String CUSTOMER_NAME = "Tom";
  private static final String CUSTOMER_SURNAME = "Acme";
  private static final Country CUSTOMER_RESIDENCE_COUNTRY = Country.NL;

  @InjectMocks
  private CustomerService customerService;

  @Mock
  private CustomerRepository customerRepository;

  @Test
  public void getCustomerById_givenCustomerIdThenReturnCustomer() {

    Optional<Customer> customer = Optional.of(Customer.builder().name(CUSTOMER_NAME).surname(CUSTOMER_SURNAME).residenceCountry(CUSTOMER_RESIDENCE_COUNTRY).build());
    when(customerRepository.findById(anyInt())).thenReturn(customer);
    Customer customerById = customerService.getCustomerById(12345);
    assertThat(customerById).isNotNull();
    assertThat(customerById).isNotNull().extracting("name", "surname", "residenceCountry").contains(CUSTOMER_NAME, CUSTOMER_SURNAME, CUSTOMER_RESIDENCE_COUNTRY);
  }

  @Test(expected = CustomerNotFoundException.class)
  public void getCustomerById_givenCustomerIdWhenCustomerNotExistThenThrowException() {
    when(customerRepository.findById(anyInt())).thenReturn(Optional.empty());
    customerService.getCustomerById(12345);
  }

  @Test(expected = NullPointerException.class)
  public void getCustomerById_givenNullCustomerIdThenThrowException() {
    customerService.getCustomerById(null);
  }

  @Test
  public void createCustomerById_givenCustomerInfoWhenCreateCustomerThenSaveCustomer() {

    Customer givenCustomer = Customer.builder().name(CUSTOMER_NAME).surname(CUSTOMER_SURNAME).residenceCountry(CUSTOMER_RESIDENCE_COUNTRY).build();
    Customer savedCustomer = Customer.builder().id(1001).name(CUSTOMER_NAME).surname(CUSTOMER_SURNAME).residenceCountry(CUSTOMER_RESIDENCE_COUNTRY).build();
    when(customerRepository.save(any(Customer.class))).thenReturn(savedCustomer);
    Customer customer = customerService.createCustomer(givenCustomer);
    assertThat(customer).isEqualTo(savedCustomer);
  }
  @Test
  public void getAllCustomers_whenGetAllCustomersThenFindAll() {
    when(customerRepository.findAll()).thenReturn(Arrays.asList(new Customer(), new Customer()));
    List<Customer> customers = customerService.getAllCustomers();
    assertThat(customers).isNotNull().hasSize(2);
  }
}
