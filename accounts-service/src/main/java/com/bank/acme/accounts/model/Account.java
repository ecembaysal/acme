package com.bank.acme.accounts.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.math.BigDecimal;
import java.time.LocalDate;

@Entity
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Account {
  /**
   * Assuming that every customer has at least one current-account by default
   */
  private Integer customerId;
  /**
   * Iban for the accounts, we assume that all account types have IBAN which is unique.
   */
  @Id
  private String iban;
  private AccountType accountType;
  private BigDecimal balance;
  private Currency currency;
  private LocalDate accountOpeningDate;
}
