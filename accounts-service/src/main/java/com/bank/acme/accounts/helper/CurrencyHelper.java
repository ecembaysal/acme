package com.bank.acme.accounts.helper;

import com.bank.acme.accounts.model.Country;
import com.bank.acme.accounts.model.Currency;
import org.springframework.util.ObjectUtils;

public class CurrencyHelper {

  public static Currency getRelevantCurrencyForCountry(Country country) {
    if (ObjectUtils.isEmpty(country)) {
      return Currency.EUR;
    }
    return country.getCurrency();
  }

}
