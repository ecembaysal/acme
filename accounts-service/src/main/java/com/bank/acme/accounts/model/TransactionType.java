package com.bank.acme.accounts.model;

/**
 * Represents the Types of each transaction.
 * Although there is {@link TransactionType#WITHDRAW} defined, it is not yet supported by our application.
 */
public enum TransactionType {
  WITHDRAW,
  DEPOSIT,
  TRANSFER
}
